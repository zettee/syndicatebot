package de.zettee.discordbot.events;

import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;

public abstract class EventHandler {

    public abstract Mono<Void> onMessageCreate(MessageCreateEvent event);

}
