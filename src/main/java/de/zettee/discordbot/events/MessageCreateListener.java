package de.zettee.discordbot.events;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import reactor.core.publisher.Mono;

public class MessageCreateListener extends EventHandler {

    @Override
    public Mono<Void> onMessageCreate(MessageCreateEvent event) {
        Message message = event.getMessage();
        return message.getContent()
                .filter(content -> content.startsWith("ts echo "))
                .map(Mono::just)
                .orElseGet(Mono::empty)
                .map(content -> content.substring("ts echo ".length()))
                .flatMap(source -> message.getChannel()
                        .flatMap(channel -> channel.createMessage(source)))
                .then();
    }
}
