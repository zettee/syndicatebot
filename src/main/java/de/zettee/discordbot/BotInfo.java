package de.zettee.discordbot;

import discord4j.core.DiscordClient;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;

public class BotInfo {

    private User owner;
    private DiscordClient discordClient;

    public BotInfo(DiscordClient discordClient){
        this.discordClient = discordClient;

        Core.getInstance().getDiscordClient().getUserById(Snowflake.of(Long.valueOf("230697828073209857"))).subscribe(this::setOwner);
    }

    public User getOwner() {
        return owner;
    }
    public DiscordClient getDiscordClient() {
        return discordClient;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
