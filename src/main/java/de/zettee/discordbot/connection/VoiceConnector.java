package de.zettee.discordbot.connection;

import de.zettee.discordbot.Core;
import discord4j.core.object.entity.VoiceChannel;
import discord4j.voice.VoiceConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VoiceConnector {
    private static Logger LOGGER = LogManager.getLogger(VoiceConnector.class);
    private static boolean DEBUG;

    private VoiceConnection voiceConnection;

    public VoiceConnector(){
        DEBUG = Core.DEBUG;
        this.voiceConnection = null;
    }

    public void connect(VoiceChannel voiceChannel){
        voiceChannel.join(spec -> spec.setProvider(Core.getInstance().getMusicManager().getProvider())).subscribe(this::setVoiceConnection);
        if(DEBUG) LOGGER.info("connect(): Joining voice channel.");
    }
    public void disconnect(){
        if(!this.isConnected()) return;
        
        this.voiceConnection.disconnect();
        this.voiceConnection = null;
        
        if(DEBUG) LOGGER.info("disconnect(): Leaving voice channel.");
    }

    public boolean isConnected(){
        return this.voiceConnection != null;
    }
    public VoiceConnection getVoiceConnection() {
        return voiceConnection;
    }
    public void setVoiceConnection(VoiceConnection voiceConnection) {
        this.voiceConnection = voiceConnection;
    }
}
