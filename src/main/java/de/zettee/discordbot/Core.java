package de.zettee.discordbot;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import de.zettee.discordbot.command.CommandRegistry;
import de.zettee.discordbot.command.commands.CMD_Help;
import de.zettee.discordbot.command.commands.music.CMD_Join;
import de.zettee.discordbot.command.commands.music.CMD_Leave;
import de.zettee.discordbot.command.commands.music.CMD_Play;
import de.zettee.discordbot.command.commands.music.CMD_Stop;
import de.zettee.discordbot.connection.VoiceConnector;
import de.zettee.discordbot.modules.music.GuildMusicManager;
import de.zettee.discordbot.modules.presence.DiscordPresence;
import de.zettee.discordbot.modules.presence.DiscordPresenceStatus;
import de.zettee.discordbot.modules.presence.DiscordPresenceType;
import de.zettee.discordbot.updater.HTTPConnector;
import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.object.entity.User;
import discord4j.core.object.presence.Presence;
import discord4j.voice.AudioProvider;
import discord4j.voice.VoiceConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Core {
    private static Logger LOGGER = LogManager.getLogger(Core.class);
    public static boolean DEBUG = true;

    private static Core instance;
    private BotInfo botInfo;
    
    private DiscordClient discordClient;
    private DiscordClientBuilder clientBuilder;
    private CommandRegistry commandRegistry;

    private AudioPlayerManager playerManager;
    private GuildMusicManager musicManager;

    private VoiceConnector voiceConnector;

    public Core(String token) throws Exception {
        if(DEBUG) LOGGER.info("Core(): constructed.");

        instance = this; //GENERATE INSTANCE OF CORE
        this.voiceConnector = new VoiceConnector();

        this.playerManager = new DefaultAudioPlayerManager();
        AudioSourceManagers.registerRemoteSources(this.playerManager);
        this.musicManager = new GuildMusicManager(this.playerManager);

        this.clientBuilder = new DiscordClientBuilder(token);
        this.commandRegistry = new CommandRegistry(DEBUG, "de.zettee.discordbot.command.commands");

        Presence presence = new Presence(new DiscordPresence().name("In Development").status(DiscordPresenceStatus.DND).type(DiscordPresenceType.PLAYING).build());
        this.clientBuilder.setInitialPresence(presence);

        this.discordClient = this.clientBuilder.build();

        //Ready event
        this.discordClient.getEventDispatcher().on(ReadyEvent.class).subscribe(readyEvent -> {
            User self = readyEvent.getSelf();
            LOGGER.info("Core(): "+String.format("Logged in as %s#%s", self.getUsername(), self.getDiscriminator()));
        });

        this.discordClient.getEventDispatcher()
                .on(MessageCreateEvent.class)
                .subscribe(event -> {
                    if(!event.getMessage().getAuthor().map(user -> !user.isBot()).orElse(false)){
                        return;
                    }
                    if(!event.getMessage().getContent().orElse("").startsWith("ts ")) {
                        return;
                    }

                    final User[] sender = {null};
                    final String[] author = {""};
                    final String[] content = {""};
                    final MessageChannel[] channel = {null};

                    event.getMessage().getAuthor().ifPresent(user -> {author[0] = user.getUsername()+"#"+user.getDiscriminator(); sender[0] = user;});
                    event.getMessage().getContent().ifPresent(cont -> content[0] = cont);
                    event.getMessage().getChannel().subscribe(channel1 -> channel[0] = channel1);

                    if (DEBUG) LOGGER.info("Core->MessageCreateEvent(): Received by '"+ author[0] +"': "+content[0]);

                    this.commandRegistry.handle(event,sender[0], content[0], channel[0]);
                });

        //SHUTDOWN HOOK
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            getVoiceConnector().disconnect();
            this.discordClient.logout();
        }));

        this.botInfo = new BotInfo(this.discordClient);
        this.discordClient.login().block();
    }

    public static void main(String[] args) throws Exception {
        LOGGER.info("main(): called.");
        if(Arrays.asList(args).contains("-debug")){
            DEBUG = true;
            LOGGER.info("main(): parameter 'debug' found.");
            LOGGER.info("main(): debugmode enabled.");
        }

        new Core("NOT FOR YOUR EYES");
    }

    public VoiceConnector getVoiceConnector() {
        return voiceConnector;
    }

    public static Core getInstance() {
        return instance;
    }
    public DiscordClient getDiscordClient() {
        return this.discordClient;
    }
    public BotInfo getBotInfo() {
        return botInfo;
    }
    public GuildMusicManager getMusicManager() {
        return musicManager;
    }
    public AudioPlayerManager getPlayerManager() {
        return playerManager;
    }
}
