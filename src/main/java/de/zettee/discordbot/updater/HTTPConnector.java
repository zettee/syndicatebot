package de.zettee.discordbot.updater;

import de.zettee.discordbot.Core;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HTTPConnector {
    private static Logger LOGGER = LogManager.getLogger(HTTPConnector.class);
    private static boolean DEBUG = false;

    public static List<String> cookies = new ArrayList<>();
    private static final String USER_AGENT = "Mozilla/5.0";

    private static String formatURL(String gateway, String... additionalParameters){
        StringBuilder url = new StringBuilder(gateway);
        for(String s : additionalParameters){
            if(s.equals(additionalParameters[0])) url.append("?").append(s); else url.append("&").append(s);
        }
        return url.toString();
    }
    private static String formatParams(String... additionalParameters){
        return formatURL("", additionalParameters);
    }

    public static void setCookies(List<String> cookies) {
        HTTPConnector.cookies = cookies;
    }
    public static List<String> getCookies() {
        return cookies;
    }

    public static BufferedReader contentStream(String gateway, String method, boolean keepSession, String... additionalParameters) throws Exception {
        DEBUG = Core.DEBUG;
        if(DEBUG) {
            LOGGER.info("json(): creating request.");
            LOGGER.info("=============================================");
            LOGGER.info("GATEWAY: " + gateway);
            LOGGER.info("REQUEST_METHOD: " + method.toUpperCase());
        }
        gateway = formatURL(gateway, additionalParameters);
        if(DEBUG) {
            LOGGER.info("PARAMETERS: "+ Arrays.asList(additionalParameters).toString());
            LOGGER.info("FULL URL: "+gateway);
            LOGGER.info("=============================================");
        }

        URL url = new URL(gateway);
        HttpURLConnection conn;

        CookieHandler.setDefault(new CookieManager());

        if (keepSession){
            conn = (HttpURLConnection) url.openConnection();
            setCookies(conn.getHeaderFields().get("Set-Cookie"));
            conn.disconnect();
        }
        conn = (HttpURLConnection) url.openConnection();

        //Act like browser
        conn.setUseCaches(false);
        conn.setRequestMethod(method.toUpperCase());
        conn.setRequestProperty("Host", "www.testpurpose.com");
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        if (keepSession && getCookies() != null) {
            for (String cookie : getCookies()) {
                conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }
        }
        conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Referer", url.getRef());
        conn.setRequestProperty("Content-Type", "application/json");      //ONLY BECAUSE JSON IS NEEDED
        conn.setRequestProperty("Content-Length", Integer.toString(additionalParameters.length));

        conn.setDoOutput(true);
        conn.setDoInput(true);

        //Send GET
        DataOutputStream dataOutputStream = new DataOutputStream(conn.getOutputStream());
        dataOutputStream.writeBytes(formatParams(additionalParameters));
        dataOutputStream.flush();
        dataOutputStream.close();

        int responseCode = conn.getResponseCode();


        if(DEBUG) {
            LOGGER.info("json(): Sending '"+method.toUpperCase()+"' request to URL : " + url);
            LOGGER.info("json(): Post parameters : " + formatParams(additionalParameters));
            LOGGER.info("json(): Response Code : " + responseCode);
        }

        return new BufferedReader(new InputStreamReader(conn.getInputStream()));
    }

    public static JSONObject json(String gateway, String method, boolean keepSession, String... additionalParameters) throws Exception {
        BufferedReader in = contentStream(gateway,method,keepSession,additionalParameters);

        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        if(DEBUG && getCookies() != null ) LOGGER.info("json(): Cookies: "+getCookies().toString());
        return (JSONObject) new JSONParser().parse(response.toString());
    }

    public static File asFile(BufferedReader in){
        return null;
    }

    public static String sendRequest(String gateway, String method, String... additionalParameters) throws Exception {
        DEBUG = Core.DEBUG;
        if(DEBUG) {
            LOGGER.info("sendRequest(): creating request.");
            LOGGER.info("=============================================");
            LOGGER.info("GATEWAY: " + gateway);
            LOGGER.info("REQUEST_METHOD: " + method.toUpperCase());

        }

        gateway = formatURL(gateway, additionalParameters);

        if(DEBUG) {
            LOGGER.info("PARAMETERS: "+ Arrays.asList(additionalParameters).toString());
            LOGGER.info(" ");
            LOGGER.info("FULL URL: "+gateway);
            LOGGER.info("=============================================");
        }

        URL url = new URL(gateway);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestProperty("Content-Type", "application/json");
        httpConnection.setRequestMethod(method.toUpperCase());

        BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
        String line;
        StringBuilder content = new StringBuilder();

        while((line = reader.readLine()) != null) {
            content.append(line);
        }

        reader.close();
        if(DEBUG) LOGGER.info("sendRequest(): Response received: "+content);
        return content.toString();
    }

    public static File download(File destination, String gateway, String... additionalParameters) throws Exception {
        if(DEBUG) {
            LOGGER.info("download(): creating request.");
            LOGGER.info("=============================================");
            LOGGER.info("GATEWAY: " + gateway);
        }

        gateway = formatURL(gateway, additionalParameters);

        if(DEBUG) {
            LOGGER.info("PARAMETERS: "+ Arrays.asList(additionalParameters).toString());
            LOGGER.info(" ");
            LOGGER.info("FULL URL: "+gateway);
            LOGGER.info("=============================================");
        }

        try (BufferedInputStream in = new BufferedInputStream(new URL(gateway).openStream());
             FileOutputStream fileOutputStream = new FileOutputStream(destination)) {
            byte[] dataBuffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                if(DEBUG) LOGGER.info("download(): bytes read: "+bytesRead);
                fileOutputStream.write(dataBuffer, 0, bytesRead);
                fileOutputStream.flush();
            }

            InputStream in2 = new URL(gateway).openStream();
            Files.copy(in2, Paths.get(destination.getName()), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ignored) { }
        
        LOGGER.info("download(): Downloaded successfully into '"+destination.getAbsolutePath()+"'.");
        return destination;
    }

}
