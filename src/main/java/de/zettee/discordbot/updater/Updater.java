package de.zettee.discordbot.updater;

import de.zettee.discordbot.Core;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class Updater {
    private static Logger LOGGER = LogManager.getLogger(Updater.class);
    @Getter private static boolean DEBUG = true;

    private String version;
    private int productID;
    private String channel;
    private final String GATEWAY = "https://update.zitzmann-cedric.de/";

    public Updater(int productID, String channel) throws Exception {
        DEBUG = Core.DEBUG;
        this.productID = productID;
        this.channel = channel;
        this.readBotInfo();
    }

    private void readBotInfo() throws Exception {
        if(DEBUG) LOGGER.info("readBotInfo(): reading.");
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("bot.json");
        JSONParser jsonParser = new JSONParser();

        if(inputStream == null) {
            LOGGER.warn("readBotInfo(): missing inputstream. can't update");
            return;
        }

        JSONObject jsonObject = (JSONObject) jsonParser.parse(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        this.version = String.valueOf(jsonObject.get("version"));
    }

    public void check() {
        new Thread(() -> {
            LOGGER.info("check(): Checking for updates.");
            try {
                JSONObject json = HTTPConnector.json(GATEWAY+"check/", "get", true,"productID=" + this.productID,
                        "version=" + this.version,
                        "channel=" + this.channel);

                if(!Boolean.valueOf(String.valueOf(json.get("status")))) {
                    LOGGER.info("check(): Failed. Message from host: "+json.get("message"));
                    Thread.currentThread().interrupt();
                    return;
                }

                if(!Boolean.valueOf(String.valueOf(json.get("available")))) {
                    LOGGER.info("check(): Done. Message from host: "+json.get("message"));
                    Thread.currentThread().interrupt();
                    return;
                }

                LOGGER.info("check(): Done. New update has been found: '"+json.get("version")+"'.");
                LOGGER.info("check(): Ready for next step.");
                download(String.valueOf(json.get("version")).split("-")[0], this.channel);
                Thread.currentThread().interrupt();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
    public void download(String version, String channel) throws Exception {
        new Thread(() -> {
            if(DEBUG) LOGGER.info("download(): called.");
            try {
                JSONObject json = HTTPConnector.json(GATEWAY+"download/", "get", true,"productID=" + this.productID, "version=" + this.version, "channel=" + this.channel);
                LOGGER.info("download(): "+json.toJSONString());

                LOGGER.info("download(): "+HTTPConnector.contentStream(GATEWAY+"download/", "get", true, "productID=" + this.productID, "version=" + this.version, "channel=" + this.channel));
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*try {
                String response = HTTPConnector.sendRequest(GATEWAY + "download/",
                        "get",
                        "productID=" + this.productID,
                        "version=" + this.version,
                        "channel=" + this.channel);


                LOGGER.info("download(): Response: " + response);
                File destination = new File(System.getProperty("user.dir") + "/update/");
                if(!destination.exists()) destination.mkdirs();

                HTTPConnector.download(destination, GATEWAY, "productID=" + this.productID, "version=" + version, "channel=" + channel);
            } catch (Exception ignored){ }*/
        }).start();
    }
    public void verify(){
        if(DEBUG) LOGGER.info("verify(): called.");

    }

}
