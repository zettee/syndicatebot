package de.zettee.discordbot.modules.presence;

public enum DiscordPresenceType {

    PLAYING(1),
    LISTENING(2),
    WATCHING(3);

    private Integer value;
    DiscordPresenceType(Integer value){
        this.value = value;
    }

    public Integer getValue(){
        return this.value;
    }

}
