package de.zettee.discordbot.modules.presence;

public enum DiscordPresenceStatus {

    DND("dnd"),
    ONLINE("online"),
    IDLE("idle");

    private String value;
    DiscordPresenceStatus(String value){
        this.value = value;
    }

    public String getValue() {return this.value; }

}
