package de.zettee.discordbot.modules.presence;

import discord4j.core.object.data.stored.ActivityBean;
import discord4j.core.object.data.stored.PresenceBean;

public class DiscordPresence {

    private String name = "";
    private DiscordPresenceStatus presenceStatus = DiscordPresenceStatus.ONLINE;
    private DiscordPresenceType presenceType = DiscordPresenceType.LISTENING;

    public DiscordPresence name(String name) {
        this.name = name;
        return this;
    }
    public DiscordPresence type(DiscordPresenceType presenceType) {
        this.presenceType = presenceType;
        return this;
    }
    public DiscordPresence status(DiscordPresenceStatus presenceStatus) {
        this.presenceStatus = presenceStatus;
        return this;
    }

    public PresenceBean build(){
        PresenceBean presenceBean = new PresenceBean();
        ActivityBean activityBean = new ActivityBean();

        activityBean.setName(this.name);
        activityBean.setType(presenceType.getValue());

        presenceBean.setActivity(activityBean);
        presenceBean.setStatus(this.presenceStatus.getValue());
        return presenceBean;
    }




}
