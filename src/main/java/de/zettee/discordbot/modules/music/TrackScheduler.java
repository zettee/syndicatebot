package de.zettee.discordbot.modules.music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import de.zettee.discordbot.Core;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TrackScheduler implements AudioLoadResultHandler {
    private static Logger LOGGER = LogManager.getLogger(TrackScheduler.class);
    private static boolean DEBUG = true;

    private final AudioPlayer player;

    public TrackScheduler(final AudioPlayer player){
        DEBUG = Core.DEBUG;
        this.player = player;
    }

    @Override
    public void trackLoaded(final AudioTrack track) {
        if(DEBUG) LOGGER.info("trackLoaded(): called.");
        // LavaPlayer found an audio source for us to play

        Core.getInstance().getMusicManager().getPlaylist().addTrack(track);

        LOGGER.info("trackLoaded(): "+Core.getInstance().getMusicManager().getPlaylist().next().getIdentifier());

        player.playTrack(Core.getInstance().getMusicManager().getPlaylist().next());

        /*if(Core.getInstance().getMusicManager().getPlayer().getPlayingTrack() == null) {
            LOGGER.info("trackLoaded(): no track playing. starting new one.");
            player.playTrack(Core.getInstance().getMusicManager().getPlaylist().next());
        }*/
        if(DEBUG) LOGGER.info("trackLoaded(): QUEUE: "+Core.getInstance().getMusicManager().getPlaylist().getTracks().toString());
    }

    @Override
    public void playlistLoaded(final AudioPlaylist playlist) {
        System.out.println("Not loaded");
        // LavaPlayer found multiple AudioTracks from some playlist

        playlist.getTracks().forEach(track -> Core.getInstance().getMusicManager().getPlaylist().addTrack(track));

        if(Core.getInstance().getMusicManager().getPlayer().getPlayingTrack() == null) player.playTrack(Core.getInstance().getMusicManager().getPlaylist().next());
    }

    @Override
    public void noMatches() {
        System.out.println("No matches");
        // LavaPlayer did not find any audio to extract
    }

    @Override
    public void loadFailed(final FriendlyException exception) {
        System.out.println("Failed Loading");
        // LavaPlayer could not parse an audio source for some reason
    }
}
