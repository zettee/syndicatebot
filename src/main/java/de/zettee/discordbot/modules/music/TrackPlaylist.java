package de.zettee.discordbot.modules.music;

import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import de.zettee.discordbot.Core;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class TrackPlaylist implements AudioPlaylist {
    private static Logger LOGGER = LogManager.getLogger(TrackPlaylist.class);
    private static boolean DEBUG = true;

    private String name;
    private List<AudioTrack> tracks;
    private AudioTrack selected;

    public TrackPlaylist(String name){
        DEBUG = Core.DEBUG;
        this.name = name;
    }

    public void addTrack(AudioTrack track){
        if(DEBUG) LOGGER.info("addTrack(): adding track.");

        if(this.tracks.size() <= 0)
            this.selected = track;
        else
            this.tracks.add(track);

        if(Core.getInstance().getMusicManager().getPlayer().getPlayingTrack() == null) {
            Core.getInstance().getMusicManager().getPlayer().playTrack(this.next());
        }
    }
    public AudioTrack next(){
        AudioTrack next = this.selected;        //RETURN CURRENT SELECTED;
        this.selected = this.tracks.get(0);     //SET NEW SELECTED OUT OF LIST
        this.tracks.remove(this.selected);      //REMOVE SELECTED FROM LIST
        return next;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<AudioTrack> getTracks() {
        return this.tracks;
    }

    @Override
    public AudioTrack getSelectedTrack() {
        return this.selected;
    }

    @Override
    public boolean isSearchResult() {
        return false;
    }
}
