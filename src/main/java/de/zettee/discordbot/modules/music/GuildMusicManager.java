package de.zettee.discordbot.modules.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;

public class GuildMusicManager {

    private final AudioPlayer player;
    private final TrackScheduler scheduler;
    private final TrackQueueManager trackQueueManager;
    private final DiscordAudioProvider provider;
    private final TrackPlaylist playlist;

    public GuildMusicManager(AudioPlayerManager manager) {
        this.player = manager.createPlayer();

        this.player.addListener(new TrackQueueManager(this.player));

        this.scheduler = new TrackScheduler(this.player);
        this.trackQueueManager = new TrackQueueManager(this.player);
        this.provider = new DiscordAudioProvider(this.player);

        this.playlist = new TrackPlaylist("GUILDQUEUE");
    }

    public AudioPlayer getPlayer() {
        return player;
    }
    public TrackPlaylist getPlaylist() {
        return playlist;
    }
    public TrackScheduler getScheduler() {
        return scheduler;
    }
    public TrackQueueManager getTrackQueueManager() {
        return trackQueueManager;
    }
    public DiscordAudioProvider getProvider() {
        return provider;
    }
}
