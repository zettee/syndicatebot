package de.zettee.discordbot.modules.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import de.zettee.discordbot.Core;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TrackQueueManager extends AudioEventAdapter {
    private static Logger LOGGER = LogManager.getLogger(TrackQueueManager.class);
    private static boolean DEBUG = true;

    private final AudioPlayer player;
    public TrackQueueManager(final AudioPlayer player){
        this.player = player;
    }

    @Override
    public void onPlayerPause(AudioPlayer player) {
        // Player was paused
        LOGGER.info("onPlayerPause(): PAUSED.");
    }

    @Override
    public void onPlayerResume(AudioPlayer player) {
        // Player was resumed
        LOGGER.info("onPlayerResume(): RESUMED.");
    }

    @Override
    public void onTrackStart(AudioPlayer player, AudioTrack track) {
        // A track started playing
        LOGGER.info("onTrackStart(): TRACK STARTED.");
    }

    @Override
    public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
        LOGGER.info("onTrackEnd(): TRACK END. Reason: "+endReason.name());
        if (endReason.mayStartNext) {
            // Start next track
            Core.getInstance().getMusicManager().getPlayer().playTrack(Core.getInstance().getMusicManager().getPlaylist().next());
            //Core.getInstance().getMusicManager().getPlaylist().getSelectedTrack();
        }

        // endReason == FINISHED: A track finished or died by an exception (mayStartNext = true).
        // endReason == LOAD_FAILED: Loading of a track failed (mayStartNext = true).
        // endReason == STOPPED: The player was stopped.
        // endReason == REPLACED: Another track started playing while this had not finished
        // endReason == CLEANUP: Player hasn't been queried for a while, if you want you can put a
        //                       clone of this back to your queue
    }

    @Override
    public void onTrackException(AudioPlayer player, AudioTrack track, FriendlyException exception) {
        // An already playing track threw an exception (track end event will still be received separately)
    }

    @Override
    public void onTrackStuck(AudioPlayer player, AudioTrack track, long thresholdMs) {
        // Audio track has been unable to provide us any audio, might want to just start a new track
    }
}
