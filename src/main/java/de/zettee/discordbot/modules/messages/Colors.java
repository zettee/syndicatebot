package de.zettee.discordbot.modules.messages;

import java.awt.*;

public enum Colors {

    ALIZARIN(new Color(231, 76, 60)),
    CARROT(new Color(230, 126, 34)),
    SUN_FLOWER(new Color(241, 196, 15)),
    EMERALD(new Color(46, 204, 113)),
    PETER_RIVER(new Color(52, 152, 219)),
    AMETHYST(new Color(155, 89, 182)),
    MIDNIGHT(new Color(44, 62, 80)),
    ASBESTOS(new Color(127, 140, 141));

    private Color value;
    Colors(Color value){
        this.value = value;
    }

    public Color value() {
        return value;
    }
}
