package de.zettee.discordbot.modules.messages;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.spec.MessageCreateSpec;
import reactor.core.publisher.Mono;

import java.io.*;
import java.util.function.Consumer;

public class MessageBuilder {

    private String content;
    private boolean tts;
    private EmbedBuilder embed = null;
    private Object[] attachment = new Object[] {};

    public MessageBuilder(String content, boolean tts) {
        this.content = content;
        this.tts = tts;
    }

    public MessageBuilder attachment(String name, InputStream file) throws Exception {
        this.attachment = new Object[] {name, new ObjectInputStream(file).readObject()};
        return this;
    }
    public MessageBuilder embed(EmbedBuilder embed) {
        this.embed = embed;
        return this;
    }
    public Consumer<? super MessageCreateSpec> build(){
        return messageSpec -> {
            messageSpec.setContent(this.content);
            messageSpec.setTts(this.tts);

            if(this.attachment.length > 0) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(baos);
                    oos.writeObject(this.attachment[1]);
                    oos.flush();
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                messageSpec.addFile(String.valueOf(this.attachment[0]),new ByteArrayInputStream(baos.toByteArray()));
            }

            if(this.embed != null) messageSpec.setEmbed(this.embed.build());
        };
    }

    public Mono<Message> send(MessageChannel channel){
        Mono<Message> message = channel.createMessage(this.build());
        message.subscribe();
        return message;
    }

}
