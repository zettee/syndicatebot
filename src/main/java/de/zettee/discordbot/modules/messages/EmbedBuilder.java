package de.zettee.discordbot.modules.messages;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.spec.EmbedCreateSpec;
import reactor.core.publisher.Mono;

import java.awt.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class EmbedBuilder {
    private String title;
    private String description;
    private String url;
    private String[] author = new String[] {};
    private Color color;
    private String[] footer = new String[] {};
    private String image;
    private String thumbnail;
    private Instant timestamp;

    private List<String[]> fields = new ArrayList<>();

    public Mono<Message> send(MessageChannel channel){
        Mono<Message> message = channel.createEmbed(this.build());
        message.subscribe();
        return message;
    }
    public Consumer<? super EmbedCreateSpec> build(){
        return embedCreateSpec -> {
            embedCreateSpec.setTitle(this.title);
            embedCreateSpec.setDescription(this.description);
            embedCreateSpec.setColor(Colors.ASBESTOS.value());

            for(String[] object : this.fields){
                embedCreateSpec.addField(object[0],object[1],Boolean.valueOf(object[2]));
            }

            if (this.url != null) embedCreateSpec.setUrl(this.url);
            if (this.author.length > 1) embedCreateSpec.setAuthor(this.author[0],this.author[1],this.author[2]);
            if (this.color != null) embedCreateSpec.setColor(this.color);
            if (this.footer.length > 0) embedCreateSpec.setFooter(this.footer[0],this.footer[1]);
            if (this.image != null) embedCreateSpec.setImage(this.image);
            if (this.thumbnail != null) embedCreateSpec.setThumbnail(this.thumbnail);
            if (this.timestamp != null) embedCreateSpec.setTimestamp(timestamp);
        };
    }

    public EmbedBuilder(String title, String description){
        this.title = title;
        this.description = description;
    }
    public EmbedBuilder addField(String name, String value, boolean inline){
        this.fields.add(new String[] {name, value, String.valueOf(inline)});
        return this;
    }
    public EmbedBuilder url(String url) {
        this.url = url;
        return this;
    }
    public EmbedBuilder author(String name, String url, String iconURL) {
        this.author = new String[] {name, url, iconURL};
        return this;
    }
    public EmbedBuilder color(Colors color) {
        this.color = color.value();
        return this;
    }
    public EmbedBuilder footer(String text, String iconURL) {
        this.footer = new String[] {text, iconURL};
        return this;
    }
    public EmbedBuilder image(String url) {
        this.image = url;
        return this;
    }
    public EmbedBuilder thumbnail(String url) {
        this.thumbnail = url;
        return this;
    }
    public EmbedBuilder timestamp(Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Color getColor() {
        return color;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public String[] getAuthor() {
        return author;
    }

    public String[] getFooter() {
        return footer;
    }
}
