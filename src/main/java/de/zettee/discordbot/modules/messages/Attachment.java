package de.zettee.discordbot.modules.messages;

import java.io.InputStream;

public class Attachment {
    private InputStream file;
    private String name;

    public Attachment(InputStream file, String name){
        this.file = file;
        this.name = name;
    }

    public InputStream getFile() {
        return file;
    }

    public String getName() {
        return name;
    }
}
