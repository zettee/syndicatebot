package de.zettee.discordbot.modules.messages;

public class Author {
    private String name;
    private String url;
    private String iconURL;

    public Author(String name, String url, String iconURL){
        this.name = name;
        this.url = url;
        this.iconURL = iconURL;
    }

    public String getName() {
        return name;
    }
    public String getIconURL() {
        return iconURL;
    }
    public String getUrl() {
        return url;
    }
}
