package de.zettee.discordbot.command;

import de.zettee.discordbot.modules.messages.Colors;
import de.zettee.discordbot.modules.messages.EmbedBuilder;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.object.entity.User;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class CommandRegistry {
    private static Logger LOGGER = LogManager.getLogger(CommandRegistry.class);
    @Getter private static boolean DEBUG;

    public HashMap<String, Command> commands = new HashMap<>();

    public CommandRegistry(boolean debug, String... packages) throws Exception {
        DEBUG = debug;
        if(DEBUG) LOGGER.info("CommandRegistry(): constructed.");
        this.registerCommands(packages);
    }

    public void handle(MessageCreateEvent event, User sender, String content, MessageChannel channel) {
        String[] args;

        args = content.replace("ts ","").split(" ");
        if(this.commands.containsKey(args[0].toLowerCase())){
            Command command = this.commands.get(args[0]);
            command.setChannel(channel);
            command.execute(event,sender, args);
        } else {
            new EmbedBuilder("404: Not Found", "Du Unterbrecherhirn hast auch noch nicht gerafft, dass es diesen Command hier nicht gibt.").color(Colors.ALIZARIN).send(channel);
        }
    }

    public void registerCommand(Class<? extends Command> clazz) {
        if(DEBUG) LOGGER.info("registerCommand(): registering command class '"+clazz.getName()+"'.");
        try {
            Command command = clazz.newInstance();
            command.getCategory().commands.add(command);
            commands.put(command.getName(), command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registerCommands(String... packages) {
        if(DEBUG) LOGGER.info("registerCommands(): registering all commands of packages "+Arrays.asList(packages).toString()+".");

        Arrays.asList(packages).forEach(p -> {
            new Reflections(p).getSubTypesOf(Command.class).forEach(this::registerCommand);
        });
    }

    private Class[] getClasses(String packageName) throws ClassNotFoundException, IOException {
        if(DEBUG) LOGGER.info("getClasses(): getting classes.");
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();

        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }

        ArrayList<Class<?>> classes = new ArrayList<>();
        for (Object directory : dirs) {
            classes.addAll(findClasses((File) directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }
    private static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException {
        if(DEBUG) LOGGER.info("findClasses(): finding classes.");
        List<Class<?>> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

}
