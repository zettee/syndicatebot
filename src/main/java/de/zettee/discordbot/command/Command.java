package de.zettee.discordbot.command;

import de.zettee.discordbot.modules.messages.Colors;
import de.zettee.discordbot.modules.messages.EmbedBuilder;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.object.entity.User;

public abstract class Command {
    private String name;
    private String usage;
    private String description;
    private Category category;
    private MessageChannel channel;

    public Command(String name, String usage, String description, Category category) {
        this.name = name;
        this.usage = usage;
        this.description = description;
        this.category = category;
    }

    public abstract void execute(MessageCreateEvent event, User sender, String[] args);

    public void syntax(){
        new EmbedBuilder(":no_entry: Syntax error!", "Did you mean **ts "+this.usage+"**").color(Colors.ALIZARIN).send(this.channel);
    }
    public void error(String error){
        new EmbedBuilder(":no_entry: Error occured!", error).color(Colors.ALIZARIN).send(this.channel);
    }

    public MessageChannel getChannel() {
        return channel;
    }
    public Category getCategory() {
        return category;
    }
    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }

    protected void setChannel(MessageChannel channel) {
        this.channel = channel;
    }
}
