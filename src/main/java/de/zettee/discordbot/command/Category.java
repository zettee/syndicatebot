package de.zettee.discordbot.command;

import lombok.Getter;

import java.util.ArrayList;

public enum Category {

    GENERAL("General",":raised_back_of_hand:", new ArrayList<>()),
    MEMES("Memes",":joy:", new ArrayList<>()),
    FUN("Fun",":stuck_out_tongue_winking_eye:", new ArrayList<>()),
    RANDOMS("Randoms",":game_die:", new ArrayList<>()),
    STATISTICS("Statistics",":part_alternation_mark:", new ArrayList<>()),
    CONFIG("Configuration",":gear:", new ArrayList<>()),
    MUSIC("Music",":notes:", new ArrayList<>());

    @Getter
    public String title;
    @Getter
    public String emoji;
    @Getter
    public ArrayList<Command> commands;

    Category(String title, String emoji, ArrayList<Command> commands){
        this.title = title;
        this.emoji = emoji;
        this.commands = commands;
    }

}
