package de.zettee.discordbot.command.commands.settings;

import de.zettee.discordbot.Core;
import de.zettee.discordbot.command.Category;
import de.zettee.discordbot.command.Command;
import de.zettee.discordbot.modules.messages.Colors;
import de.zettee.discordbot.modules.messages.EmbedBuilder;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class CMD_Prefix extends Command {
    private static Logger LOGGER = LogManager.getLogger(CMD_Prefix.class);
    private static boolean DEBUG = true;


    public CMD_Prefix() {
        super("setprefix", "setprefix <prefix>", "Change command prefix", Category.CONFIG);
        DEBUG = Core.DEBUG;
        if(DEBUG) LOGGER.info("CMD_Prefix(): constructed.");
    }

    @Override
    public void execute(MessageCreateEvent event, User sender, String[] args) {
        if(args.length != 2) {
            super.syntax();
            return;
        }
        if(args[1].length() > 3) {
            super.error("A prefix should be short, not longer than 3 chars.");
            return;
        }

        String prefix = args[1];
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("bot.conf");
        JSONParser jsonParser = new JSONParser();

        if(inputStream == null) {
            LOGGER.warn("execute(): Can't read config file.");
            super.error("Sorry, can't edit that currently.");
            return;
        }

        try {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(new InputStreamReader(inputStream, StandardCharsets.UTF_8));

            String old = String.valueOf(jsonObject.get("prefix"));
            jsonObject.put("prefix", prefix);

            FileWriter writer = new FileWriter(new File(getClass().getResource("bot.conf").toURI()));
            jsonObject.writeJSONString(writer);

            new EmbedBuilder("Success!","Changed prefix from '"+old+"' to '"+prefix+"'.").color(Colors.EMERALD).send(getChannel());
        } catch (Exception ignored){
            super.error("Sorry, can't edit that currently.");
        }

    }
}
