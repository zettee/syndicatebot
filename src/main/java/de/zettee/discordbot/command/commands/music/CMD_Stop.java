package de.zettee.discordbot.command.commands.music;

import de.zettee.discordbot.Core;
import de.zettee.discordbot.command.Category;
import de.zettee.discordbot.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;

public class CMD_Stop extends Command {
    public CMD_Stop() {
        super("stop", "stop", "Stops the playlist", Category.MUSIC);
    }

    @Override
    public void execute(MessageCreateEvent event, User sender, String[] args) {
        if(args.length != 1) {
            super.syntax();
            return;
        }

        if(Core.getInstance().getMusicManager().getPlayer().getPlayingTrack() == null) {
            super.error("");
            return;
        }

        Core.getInstance().getMusicManager().getPlayer().getPlayingTrack().stop();
    }
}
