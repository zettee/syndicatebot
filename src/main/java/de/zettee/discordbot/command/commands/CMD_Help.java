package de.zettee.discordbot.command.commands;

import de.zettee.discordbot.Core;
import de.zettee.discordbot.command.Category;
import de.zettee.discordbot.command.Command;
import de.zettee.discordbot.modules.messages.Colors;
import de.zettee.discordbot.modules.messages.EmbedBuilder;
import de.zettee.discordbot.modules.messages.MessageBuilder;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CMD_Help extends Command {
    private static Logger LOGGER = LogManager.getLogger(CMD_Help.class);
    @Getter private static boolean DEBUG = true;

    public CMD_Help() {
        super("help", "help", "Helpcenter", Category.GENERAL);
        DEBUG = Core.DEBUG;
        if(DEBUG) LOGGER.info("CMD_Help(): constructed.");
    }

    @Override
    public void execute(MessageCreateEvent event, User sender, String[] args) {
        new MessageBuilder(":calling: "+sender.getMention()+", I've sent you the instructions via **PM**.", false).send(getChannel());

        EmbedBuilder embedBuilder = new EmbedBuilder("SyndicateBot - How to control this bot", "Just for you, the nicest overview of all commands. Enjoy this honour. **not**!\n\u200B").color(Colors.EMERALD);
        embedBuilder.footer("Brought to you by: "+Core.getInstance().getBotInfo().getOwner().getUsername()+"#"+Core.getInstance().getBotInfo().getOwner().getDiscriminator(), Core.getInstance().getBotInfo().getOwner().getAvatarUrl());

        for(Category category : Category.values()){
            if(category.commands.size() > 0) {
                StringBuilder s = new StringBuilder();
                for (Command cmd : category.commands) {
                    s.append("**ts ").append(cmd.getName()).append("** - ").append(cmd.getDescription()).append("\n");
                }
                embedBuilder.addField(category.emoji + " " + category.title, s.toString()+"\n\u200B", true);
            }
        }
        sender.getPrivateChannel().subscribe(embedBuilder::send);
    }
}
