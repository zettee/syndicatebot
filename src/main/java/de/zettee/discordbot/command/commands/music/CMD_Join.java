package de.zettee.discordbot.command.commands.music;

import de.zettee.discordbot.Core;
import de.zettee.discordbot.command.Category;
import de.zettee.discordbot.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.VoiceState;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.VoiceChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CMD_Join extends Command {
    private static Logger LOGGER = LogManager.getLogger(CMD_Join.class);
    private static boolean DEBUG = true;

    public CMD_Join() {
        super("join", "join", "Join a channel", Category.MUSIC);
        DEBUG = Core.DEBUG;
        if(DEBUG) LOGGER.info("CMD_Join(): constructed.");
    }

    @Override
    public void execute(MessageCreateEvent event, User sender, String[] args) {
        if(args.length == 1) {
            if(Core.getInstance().getVoiceConnector().isConnected()) {
                super.error("Already in a channel!");
                return;
            }

            final Member member = event.getMember().orElse(null);
            if (member == null) return;
            final VoiceState voiceState = member.getVoiceState().block();

            if(voiceState == null) {
                super.error("Can't find the voice channel. Are you connected to one?");
                return;
            }

            final VoiceChannel voiceChannel = voiceState.getChannel().block();
            if(voiceChannel == null) {
                super.error("Can't find your voice channel.");
                return;
            }

            Core.getInstance().getVoiceConnector().connect(voiceChannel);
            return;
        }
        super.syntax();
    }
}
