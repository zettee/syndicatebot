package de.zettee.discordbot.command.commands.music;

import com.sedmelluq.discord.lavaplayer.tools.io.MessageInput;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import de.zettee.discordbot.Core;
import de.zettee.discordbot.command.Category;
import de.zettee.discordbot.command.Command;
import de.zettee.discordbot.modules.music.TrackPlaylist;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.VoiceState;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.VoiceChannel;

import java.net.MalformedURLException;
import java.net.URL;

public class CMD_Play extends Command {
    public CMD_Play() {
        super("play","play [url]","Plays a track", Category.MUSIC);
    }
    @Override
    public void execute(MessageCreateEvent event, User sender, String[] args) {
        if(args.length < 2) {
            super.syntax();
            return;
        }

        try {
            new URL(args[1]);
            if(!Core.getInstance().getVoiceConnector().isConnected()) {
                if(Core.getInstance().getVoiceConnector().isConnected()) {
                    super.error("Already in a channel!");
                    return;
                }

                final Member member = event.getMember().orElse(null);
                if (member == null) return;
                final VoiceState voiceState = member.getVoiceState().block();

                if(voiceState == null) {
                    super.error("Can't find the voice channel. Are you connected to one?");
                    return;
                }

                final VoiceChannel voiceChannel = voiceState.getChannel().block();
                if(voiceChannel == null) {
                    super.error("Can't find your voice channel.");
                    return;
                }

                Core.getInstance().getVoiceConnector().connect(voiceChannel);
            }

            if(Core.getInstance().getMusicManager().getPlayer().getPlayingTrack() != null){
                //CURRENTLY A TRACK IS PLAYING
                super.error("Adding track to queue.");
                //TrackPlaylist playlist = Core.getInstance().getMusicManager().getPlaylist().addTrack(Core.getInstance().getPlayerManager().decodeTrack(new MessageInput(event.getMessage().getContent())).decodedTrack);

                return;
            }

            Core.getInstance().getPlayerManager().loadItem(args[1], Core.getInstance().getMusicManager().getScheduler());
            //ADD TO QUEUE
        } catch (MalformedURLException ignored) {
            super.error("Track cannot be added due to a malformed URL.");
        }


    }
}
