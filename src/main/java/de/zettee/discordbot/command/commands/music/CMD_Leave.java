package de.zettee.discordbot.command.commands.music;

import de.zettee.discordbot.Core;
import de.zettee.discordbot.command.Category;
import de.zettee.discordbot.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CMD_Leave extends Command {
    private static Logger LOGGER = LogManager.getLogger(CMD_Leave.class);
    private static boolean DEBUG = true;

    public CMD_Leave() {
        super("leave", "leave", "Leave a channel", Category.MUSIC);
        DEBUG = Core.DEBUG;
        if(DEBUG) LOGGER.info("CMD_Leave(): constructed.");
    }

    @Override
    public void execute(MessageCreateEvent event, User sender, String[] args) {
        if(!Core.getInstance().getVoiceConnector().isConnected()) {
            super.error("I can't leave channel I'm not in, **retard**.");
            return;
        }

        Core.getInstance().getVoiceConnector().disconnect();
    }
}
